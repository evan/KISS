from setuptools import find_packages, setup

setup(
    name="kiss",
    description="Kindle Information ScreenShot",
    author="Evan Reichard",
    version="0.0.1",
    packages=find_packages(exclude=("tests",)),
    zip_safe=False,
    include_package_data=True,
    entry_points={"console_scripts": ["kiss = kiss:cli"]},
    install_requires=[
        "Pillow",
        "Flask>=2.2",
    ],
    tests_require=["pytest"],
    extras_require={"dev": ["pre-commit", "black", "flake8", "pytest"]},
)

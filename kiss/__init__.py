import sys

import click
from flask.cli import FlaskGroup


def init():
    global __version__
    global app
    global ssmanager

    import signal
    from importlib.metadata import version

    from flask import Flask

    from kiss.ssmanager import ScreenshotManager

    __version__ = version("kiss")

    # Initialize App
    app = Flask(__name__)

    # Screenshot Manager
    ssmanager = ScreenshotManager()

    # Handle SIGINT
    signal.signal(signal.SIGINT, signal_handler)


def signal_handler(sig, frame):
    if ssmanager:
        ssmanager.stop()
    sys.exit(0)


def create_app():
    init()

    import kiss.api as api_common

    app.register_blueprint(api_common.bp)

    ssmanager.start()

    return app


@click.group(cls=FlaskGroup, create_app=create_app)
def cli():
    """Management script for the application."""

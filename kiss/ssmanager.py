import os
import subprocess
import tempfile
import time
from pathlib import Path
from threading import Thread

import yaml
from PIL import Image, ImageOps


class ScreenshotManager:
    def __init__(self):
        self.__config_path = os.environ.get("CONFIG_FILE", "/data/config.yml")

        if not Path(self.__config_path).is_file():
            raise ValueError("Invalid Configuration")

        cf = open(self.__config_path, "r")
        self.__config_yaml = yaml.safe_load(cf)
        self.data_path = tempfile.gettempdir()

    def stop(self):
        self.__pending_shutdown = True
        self.__loop_thread.join()

    def start(self):
        self.__pending_shutdown = False
        self.__loop_thread = Thread(target=self.__loop)
        self.__loop_thread.start()

    def __process_image(self, ss):
        filename = ss.get("output")
        fp = Path(self.data_path, filename)
        img = ImageOps.grayscale(Image.open(fp))

        if ss.get("rotate", False) is True:
            img = img.rotate(90, expand=True)

        img.save(fp)

    def __loop(self):
        counter = 0
        while not self.__pending_shutdown:
            if counter % 900 == 0:
                subprocess.run(
                    ["shot-scraper", "multi", self.__config_path], cwd=self.data_path
                )
                for ss in self.__config_yaml:
                    self.__process_image(ss)

            time.sleep(1)
            counter += 1

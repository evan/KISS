from flask import Blueprint, send_from_directory

import kiss

# Setup blueprint
bp = Blueprint("common", __name__)


# Serve Screenshots
@bp.route("/<path:path>")
def assets(path):
    return send_from_directory(kiss.ssmanager.data_path, path)

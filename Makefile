shell:
	/bin/bash

setup_dev:
	pip3 install -e .

run_prod: setup_dev
	PYTHONUNBUFFERED=TRUE \
		gunicorn "kiss:create_app()" --bind 0.0.0.0:5000 --threads=100

run_dev: setup_dev
	PYTHONUNBUFFERED=TRUE \
		gunicorn --reload "kiss:create_app()" --bind 0.0.0.0:5000 --threads=100

docker_build_release_beta:
	docker buildx build \
		--platform linux/amd64,linux/arm64 \
		-t gitea.va.reichard.io/reichard/kiss:beta --push .

docker_build_release_latest:
	docker buildx build \
		--platform linux/amd64,linux/arm64 \
		-t gitea.va.reichard.io/reichard/kiss:latest --push .

# docker buildx imagetools create gitea.va.reichard.io/reichard/kiss:latest --tag gitea.va.reichard.io/reichard/kiss:0.0.1

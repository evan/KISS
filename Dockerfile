FROM mcr.microsoft.com/playwright:v1.28.0-focal

# Install Dependencies
RUN apt-get update -y
RUN apt-get install -y python3-pip python-setuptools
RUN pip install shot-scraper gunicorn

# Copy App
COPY . /app

# Install Module & Requirements
WORKDIR /app
RUN python3 setup.py clean --all install

# Remove Source
# WORKDIR /
# RUN rm -rf /app

# Entrypoint
ENTRYPOINT ["gunicorn"]
CMD ["kiss:create_app()", "--bind", "0.0.0.0:5000", "--threads=100"]
